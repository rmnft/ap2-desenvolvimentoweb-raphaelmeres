let container = document.querySelector('.container')


//Pegar jogadores
function mostrarCards(){
    for (let i = 0; i <= jogadores.length; i++) {
        //Usar crase
        container.innerHTML += `
            <div class="box">
                <img src="${jogadores[i]["imagem"]}">
                <h3 id="nome">${jogadores[i]["nome"]}</h3>
                <h4>${jogadores[i]["posicao"]}</h3>
                <button onclick='salvarCookie(${JSON.stringify(jogadores[i])})'> 
                    Ver mais 
                </button>
            </div>`;

    }
}

//Filtro
function usarFiltro(){
    const filtro = document.getElementById('barraDeFiltro').value.toLowerCase()
    const cardDoJogador = document.getElementsByClassName('box')

    for(let i = 0; i < cardDoJogador.length; i++){
        let nomeDoJogador = cardDoJogador[i].querySelector(".box #nome")

        if(nomeDoJogador.innerText.toLowerCase().indexOf(filtro) > -1){
            cardDoJogador[i].style.display = ""
        }else{
            cardDoJogador[i].style.display = "none"
        }
    }
}

function salvarCookie(jogador){
    document.cookie = jogador["nome"] + "$";
    document.cookie += jogador["posicao"] + "$";
    document.cookie += jogador["imagem"] + "$";
    document.cookie += jogador["nome_completo"] + "$";
    document.cookie += jogador["nascimento"] + "$";
    document.cookie += jogador["altura_peso"] + "$";

    window.location.href = './src/detalhes.html'

    document.cookie = ''
}

//Carregando a funcao
window.onload = () => {
    mostrarCards()
}